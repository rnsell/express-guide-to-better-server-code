# Dependency Injection

Depency injection and IoC allows developers to defer implementation details by passing them in at run time.

The simplist form of DI is a function that takes in another function or an object with a method on it. Additionally, one can simulate this by having a class and passing in the needed functions / objects in the constructor.

```
const info = (logger) => (message) => {
    logger.info(message);
}

function LogService (options) => {
    this.logger = options.logger
}
LogService.prototype.info = (message) => {
    this.logger.info(message)
};
```

DI has an important property for testing. Since there isn't a specific implementation until run time, it easily allows for unit testing to inject mocked functionality whether it is functions or objects with appropriate methods.

In Express it's important to be aware of these concepts to create testable routes. And the functions that build those routes. One common mistake would be to directly include a DB model via a require and import statement. This not only creates a coupling directly to the file path of the DB model but additionally means it can't be mocked.

DI in express is primarily done in two ways. Create higher order functions / Classes that return route objects and then defer invocation during application startup / setup. The other way to deal with this is using a dependency middleware to append necessary dependencies.

It should be noted higher order routes is more clear as the depedencies are expressed in the function definition. The disadvantage of this method is it generates rather ugly looking code at setup which is done once. On the other hand a centralized dependency object is a lot easier to setup and build and attach but can be unclear whats needed inside the express route / testing if proper linting / hinting doesn't exist. 

### Higher Order Routes
```
const generateRoute$H = (dependencies) => {
    const router = new Router();
    router.get("/", (req, res) => {
        const { dep1, dep2, ...otherDeps }  = dependencies;
    })

    return router;
};
```

### Dependency Middleware
```
const dependencyMiddleware$H = (dependencies) => (req, res, next) => {
    req.dependencies = dependencies;
    next()
}

const someDependency = {
    callDb = () => { ... }
}

const dependencyMiddleware = dependencyMiddleware$H(someDependency);

const app = express();
app.use(dependencyMiddleware);

// Assume a router has been defined
const newRoute = new Router();

newRoute.get("/", (req, res) => {
    const { callDb } = req.dependencies
    // Other express route information
});
```




